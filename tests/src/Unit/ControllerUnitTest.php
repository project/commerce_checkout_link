<?php

namespace Drupal\Tests\commerce_checkout_link\Unit;

use Drupal\commerce_cart\CartProviderInterface;
use Drupal\commerce_cart\CartSessionInterface;
use Drupal\commerce_checkout\CheckoutOrderManagerInterface;
use Drupal\commerce_checkout_link\CheckoutLinkManager;
use Drupal\commerce_checkout_link\Controller\CommerceCheckoutLinkController;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_order\OrderAssignmentInterface;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Routing\UrlGenerator;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Site\Settings;
use Drupal\Tests\UnitTestCase;
use Drupal\user\Entity\User;
use Drupal\user\UserStorageInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Test.
 *
 * @group commerce_checkout_link
 */
class ControllerUnitTest extends UnitTestCase {

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();
    $container = new ContainerBuilder();
    $url_generator = $this->createMock(UrlGenerator::class);
    $url_generator->method('generateFromRoute')
      ->willReturnCallback(function ($route_name, $route_parameters, $options) {
        return 'http://example.com/' . $route_name;
      });
    $container->set('url_generator', $url_generator);
    $config_factory = $this->createMock(ConfigFactoryInterface::class);
    $config = $this->createMock(ImmutableConfig::class);
    $config_factory->method('get')
      ->willReturn($config);
    $container->set('config.factory', $config_factory);
    \Drupal::setContainer($container);
  }

  /**
   * Test the controller in different ways.
   *
   * @dataProvider getControllerVariations
   */
  public function testController($timestamp, $hash, $should_redirect, $exception_message = '') {
    $order = $this->getOrder();
    $mock_time = $this->createMock(TimeInterface::class);
    $mock_time->method('getCurrentTime')
      ->willReturn(time());
    $mock_user = $this->createMock(AccountProxyInterface::class);
    $mock_cs = $this->createMock(CartSessionInterface::class);
    $mock_oa = $this->createMock(OrderAssignmentInterface::class);
    $mock_etm = $this->createMock(EntityTypeManagerInterface::class);
    $user_storage = $this->createMock(UserStorageInterface::class);
    $mock_user_object = $this->createMock(User::class);
    $mock_lf = $this->createMock(LoggerChannelFactoryInterface::class);
    $mock_cart_provider = $this->createMock(CartProviderInterface::class);
    $mock_cart_provider->method('getCarts')
      ->willReturn([]);
    $mock_mh = $this->createMock(ModuleHandlerInterface::class);
    $mock_log = $this->createMock(LoggerInterface::class);
    $mock_lf->method('get')->willReturn($mock_log);
    $user_storage->method('load')
      ->willReturn($mock_user_object);
    $mock_etm->method('getStorage')
      ->willReturn($user_storage);
    $mock_com = $this->createMock(CheckoutOrderManagerInterface::class);
    $mock_ed = $this->createMock(EventDispatcherInterface::class);
    $mock_ed->method('dispatch')
      ->willReturnArgument(0);
    $controller = new CommerceCheckoutLinkController($mock_time, $mock_user, $mock_cs, $mock_oa, $mock_etm, $mock_com, $mock_cart_provider, $mock_mh, $mock_lf, $mock_ed);
    if (!$should_redirect) {
      if ($exception_message) {
        $this->expectExceptionMessage($exception_message);
      }
      $this->expectException(AccessDeniedHttpException::class);
    }
    /** @var \Symfony\Component\HttpFoundation\RedirectResponse $return_value */
    $return_value = $controller->checkout($order, $timestamp, $hash);
    if ($should_redirect) {
      $this->assertEquals('http://example.com/commerce_checkout.form', $return_value->getTargetUrl());
    }
  }

  /**
   * Helper.
   */
  protected function getOrder() {
    $order = $this->createMock(OrderInterface::class);
    $order->method('getChangedTime')
      ->willReturn(strtotime('00:02'));
    return $order;
  }

  /**
   * A dataprovider.
   */
  public function getControllerVariations() {
    $current_time = time();
    new Settings([
      'hash_salt' => 'my_test',
    ]);
    return [
      [1, 'abc', FALSE, 'The link provided is no longer valid'],
      [$current_time, '', FALSE, 'The hash provided was not valid'],
      [
        $current_time,
        'DummyHash',
        FALSE,
        'The hash provided did not match the expected hash',
      ],
      [
        $current_time,
        CheckoutLinkManager::generateHash($current_time, $this->getOrder()),
        TRUE,
        '',
      ],
    ];
  }

}
