<?php

namespace Drupal\Tests\commerce_checkout_link\Kernel;

use Drupal\commerce_checkout_link\CheckoutLinkManager;
use Drupal\commerce_checkout_link\Controller\CommerceCheckoutLinkController;
use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_price\Price;
use Drupal\commerce_product\Entity\Product;
use Drupal\commerce_product\Entity\ProductVariation;
use Drupal\commerce_product\Entity\ProductVariationType;
use Drupal\Core\Routing\RouteObjectInterface;
use Drupal\Core\Url;
use Drupal\profile\Entity\Profile;
use Drupal\Tests\commerce_cart\Kernel\CartKernelTestBase;
use Symfony\Component\HttpFoundation\Request;

/**
 * Test.
 *
 * @group commerce_checkout_link
 */
class RedirectTest extends CartKernelTestBase {

  /**
   * The order assignment service.
   *
   * @var \Drupal\commerce_order\OrderAssignmentInterface
   */
  protected $orderAssignment;

  /**
   * A test order.
   *
   * @var \Drupal\commerce_order\Entity\OrderInterface
   */
  protected $order;

  /**
   * A test user.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $user;

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'entity_reference_revisions',
    'path',
    'profile',
    'state_machine',
    'commerce_product',
    'commerce_order',
    'commerce_payment',
    'commerce_payment_example',
    'commerce_checkout',
    'commerce_number_pattern',
    'commerce_checkout_link',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installEntitySchema('profile');
    $this->installEntitySchema('commerce_order');
    $this->installEntitySchema('commerce_order_item');
    $this->installEntitySchema('commerce_product');
    $this->installEntitySchema('commerce_product_variation');
    $this->installConfig(['commerce_product',
      'commerce_order',
      'commerce_checkout',
    ]);
    $this->user = $this->createUser(['mail' => $this->randomString() . '@example.com']);

    // Turn off title generation to allow explicit values to be used.
    $variation_type = ProductVariationType::load('default');
    $variation_type->setGenerateTitle(FALSE);
    $variation_type->save();

    $product = Product::create([
      'type' => 'default',
      'title' => 'Default testing product',
    ]);
    $product->save();

    $variation = ProductVariation::create([
      'type' => 'default',
      'product_id' => $product->id(),
      'sku' => 'TEST_' . strtolower($this->randomMachineName()),
      'title' => $this->randomString(),
      'price' => new Price('12.00', 'USD'),
      'status' => TRUE,
    ]);
    $variation->save();

    $profile = Profile::create([
      'type' => 'customer',
      'address' => [
        'country_code' => 'US',
        'postal_code' => '53177',
        'locality' => 'Milwaukee',
        'address_line1' => 'Pabst Blue Ribbon Dr',
        'administrative_area' => 'WI',
        'given_name' => 'Frederick',
        'family_name' => 'Pabst',
      ],
    ]);
    $profile->save();
    $profile = $this->reloadEntity($profile);

    /** @var \Drupal\commerce_order\OrderItemStorageInterface $order_item_storage */
    $order_item_storage = $this->container->get('entity_type.manager')
      ->getStorage('commerce_order_item');
    $order_item = $order_item_storage->createFromPurchasableEntity($variation);
    $order_item->save();

    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = Order::create([
      'type' => 'default',
      'order_number' => '6',
      'store_id' => $this->store->id(),
      'uid' => $this->user->id(),
      'mail' => $this->user->getEmail(),
      'ip_address' => '127.0.0.1',
      'billing_profile' => $profile,
      'order_items' => [$order_item],
      'state' => 'draft',
    ]);
    $order->save();
    $this->order = $this->reloadEntity($order);

    // Fake a request so that the current_route_match works.
    // @todo Remove this when CheckoutFlowBase stops using the route match.
    $url = Url::fromRoute('commerce_checkout.form', [
      'commerce_order' => $order->id(),
    ]);
    $route_provider = $this->container->get('router.route_provider');
    $route = $route_provider->getRouteByName($url->getRouteName());
    $request = Request::create($url->toString());
    $request->attributes->add([
      RouteObjectInterface::ROUTE_OBJECT => $route,
      'commerce_order' => $order,
    ]);
    $this->container->get('request_stack')->push($request);
  }

  /**
   * Test that the subscriber is swapped.
   */
  public function testRedirect() {
    $url = CheckoutLinkManager::generateUrl($this->order);
    $controller = CommerceCheckoutLinkController::create($this->container);
    $params = $url->getRouteParameters();
    $this->drupalSetCurrentUser($this->user);
    $redirect_link = $controller->checkout($this->order, $params['timestamp'], $params['hash']);
    $this->assertStringContainsString(sprintf("checkout/%d", $this->order->id()), $redirect_link->getTargetUrl());
  }

  /**
   * Test that we can still use the link, even if the order changed.
   */
  public function testRedirectWithChangedTime() {
    $this->config('commerce_checkout_link.settings')->set('use_changed_timestamp', 0)->save();
    $url = CheckoutLinkManager::generateUrl($this->order, FALSE);
    // Now re-save the order, meaning that we should be able to view it still,
    // given config.
    $this->order->save();
    $controller = CommerceCheckoutLinkController::create($this->container);
    $params = $url->getRouteParameters();
    $this->drupalSetCurrentUser($this->user);
    $redirect_link = $controller->checkout($this->order, $params['timestamp'], $params['hash']);
    $this->assertStringContainsString(sprintf("checkout/%d", $this->order->id()), $redirect_link->getTargetUrl());
  }

  /**
   * Test that we can still use a hash even if the setting changed.
   *
   * For example, if we first generate a hash, then change the settings, then
   * someone uses that link, still valid since noone changed the order, we
   * should allow them to.
   */
  public function testRedirectWithChangedTimeHashDisabledChangedTime() {
    $this->config('commerce_checkout_link.settings')->set('use_changed_timestamp', 0)->save();
    $url = CheckoutLinkManager::generateUrl($this->order);
    $controller = CommerceCheckoutLinkController::create($this->container);
    $params = $url->getRouteParameters();
    $this->drupalSetCurrentUser($this->user);
    $redirect_link = $controller->checkout($this->order, $params['timestamp'], $params['hash']);
    $this->assertStringContainsString(sprintf("checkout/%d", $this->order->id()), $redirect_link->getTargetUrl());
  }

  /**
   * Test that the URL is a valid renderable URL.
   */
  public function testUrlRender() {
    $url = CheckoutLinkManager::generateUrl($this->order);
    $url->toString(TRUE);
  }

}
